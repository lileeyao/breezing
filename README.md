Breezing Project
===============
This app is a demo working with Breezing product of metabolism device.
For product information plz refer to http://breezing.com/

[![NPM version](https://badge.fury.io/js/xss.png)](http://badge.fury.io/js/xss)

https://www.youtube.com/watch?v=4ss7682mgsc

![Alt text](http://breezing.com/wordpress/wp-content/themes/breezing/images/iphone-screen-01.png)

Limitations
-----------


Feature ideas
-------------


Contributing code
-----------------
Free to contribute.

Donations
---------


Disclaimer
----------
Do not involve it in commercial usage!

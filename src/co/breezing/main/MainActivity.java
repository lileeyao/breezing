package co.breezing.main;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;
import co.breezing.metabolism.parameter.Parameter;
import co.breezing.module.R;
import co.breezing.module.eight.flowcalc.FlowCalculation;
import co.breezing.module.eleven.reerq.REERQCalc;
import co.breezing.module.five.adaptionReference.AdaptionReference;
import co.breezing.module.four.guidance.Biofeedback;
import co.breezing.module.four.guidance.Guidance;
import co.breezing.module.nine.absorbcalc.AbsorbanceCalc;
import co.breezing.module.one.qrcode.QRcodeFromFile;
import co.breezing.module.one.qrcode.QRcodeParse;
import co.breezing.module.one.qrcode.SaveQRFile;
import co.breezing.module.one.qrcode.thirdlib.capture.CaptureActivity;
import co.breezing.module.seven.dataTranslation.DataTranslation;
import co.breezing.module.seven.dataTranslation.RawData;
import co.breezing.module.six.bluetooth.Bluetooth;
import co.breezing.module.ten.concentration.ConcentrationCalc;
import co.breezing.module.three.adaptionSuggestion.AdaptionSuggestion;
import co.breezing.module.two.pdcheck.PDCheck;

public class MainActivity extends Activity {

	public Button connectButton;
	public Button disconnectButton;

	// ////////////////////////////////////////////////////
	private ToggleButton btnStart;
	private ListView listView;
	private ListView listView_bonded;
 	private BluetoothAdapter mBluetoothAdapter;
	private SingBroadcastReceiver mReceiver;
 	private static ArrayAdapter<String> adtDevices;
 	private static ArrayAdapter<String> adtDevices_bonded;
	private static String keyName;
	private static Boolean isDone = true;
	List<String> lstDevices = new ArrayList<String>();
	List<String> lstDevices_bonded = new ArrayList<String>();
	
	private static Set<BluetoothDevice> bondedDevices;

	private static ProgressDialog pd;

	// bluetooth connection status
	public boolean connectionEstablished = false;
	public int connectionTime = 0;
	private static int pd_check_result;

	// tag
	private static String tag = "Metabolism";

	public static Timer timer;
	private static int index = 0;

	public static String qrcode;
	public static int flag;

	private int cycle_number = 0;

	public static Biofeedback biofeedback;

	private static ImageView img;
	private static AnimationDrawable animation;

	private Bluetooth bluetooth = new Bluetooth();
	private DataTranslation data = new DataTranslation();
	private QRcodeFromFile qrcodeFromFile = new QRcodeFromFile();
	private QRcodeParse qrcodeParse = new QRcodeParse();
	private RawData rawData = new RawData();
	private FlowCalculation flowCalculation = new FlowCalculation();
	private AdaptionReference adaptionRef = new AdaptionReference();
	private AdaptionSuggestion adaptionAlgo = new AdaptionSuggestion();
	private AbsorbanceCalc absorbanceCalc = new AbsorbanceCalc();
	private ConcentrationCalc concentrationCalc = new ConcentrationCalc();
	private QRcodeParse qrData = new QRcodeParse();
	private REERQCalc reeRqCal = new REERQCalc();
	private Guidance guidance = new Guidance();

	private AbsorbanceCalc delta_data = new AbsorbanceCalc();

	private long test_finish_time;

	private class SingBroadcastReceiver extends BroadcastReceiver {


	    public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction(); //may need to chain this to a recognizing function
	        if (BluetoothDevice.ACTION_FOUND.equals(action)){
	            String action1 = intent.getAction();
	            Bundle b = intent.getExtras();
	            Object[] lstName = b.keySet().toArray();
	            listView = (ListView)findViewById(R.id.listView1);
	            listView_bonded = (ListView)findViewById(R.id.listView2);
         	   	//lstDevices.add(device.getAddress());
	            // display alll the messages being received.
	            adtDevices = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, lstDevices);
	            adtDevices_bonded = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, lstDevices_bonded);
	            if (isDone) {
		            for (int i = 0; i < bondedDevices.size(); i++) {
		            	String nameString = bondedDevices.toArray()[i].toString();
		            	adtDevices_bonded.add(nameString);
	            		
	 	               
			            if (lstDevices_bonded.indexOf(nameString) == -1) {
			            	lstDevices_bonded.add(nameString);
			            }
			            listView_bonded.setAdapter(adtDevices_bonded);
		            }
		            isDone = false;
		            
	            }

	            for (int i = 0; i < lstName.length; i++) {
	            	keyName = lstName[i].toString();
	            	
	            	if(keyName.equals("android.bluetooth.device.extra.DEVICE")) {
	            		String nameString = String.valueOf(b.get("android.bluetooth.device.extra.DEVICE"));	           
	            		adtDevices.add(nameString);
	            		String str = nameString;// + " - " + deviceString;
	               
		               if (lstDevices.indexOf(str) == -1) {
		            	   lstDevices.add(str);
		               }
	            	}
	            }
			
	            listView.setAdapter(adtDevices);
				

	            // Get the BluetoothDevice object from the Intent
	            //BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	            // Add the name and address to an array adapter to show in a Toast
	            //String derp = device.getName() + " - " + device.getAddress();
	            //Toast.makeText(context, derp, Toast.LENGTH_LONG);
	            //put it on a listview.   	
	        }
	    }
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		btnStart = (ToggleButton)findViewById(R.id.toggleButton1);
		btnStart.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					mBluetoothAdapter = bluetooth.getBluetoothAdapter();
					bondedDevices = mBluetoothAdapter.getBondedDevices();
					mReceiver = new SingBroadcastReceiver();
					IntentFilter ifilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
					registerReceiver(mReceiver, ifilter);
//					Log.d(BLUETOOTH_SERVICE, "start LeScan");
					mBluetoothAdapter.startDiscovery();
					
					//mBluetoothAdapter.startLeScan(mLeScanCallback);
				}
				else {
				//	Log.d(BLUETOOTH_SERVICE, "stop LeScan");
					//mBluetoothAdapter.stopLeScan(mLeScanCallback);
				}
				//Log.d(BLUETOOTH_SERVICE, Boolean.toString(isChecked));
				//lstDevices.clear();
				//mReceiver.abortBroadcast();
			}
		});

		connectButton = (Button) findViewById(R.id.connectButton);
		connectButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (System.currentTimeMillis() >= test_finish_time + 300000) {
					new AlertDialog.Builder(MainActivity.this)
							.setTitle("Breezing")
							.setMessage("Have you scanned QR code?")
							.setCancelable(false)
							.setNeutralButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialoginterface,
												int i) {

											try {
												new AlertDialog.Builder(
														MainActivity.this)
														.setTitle(
																"Scan QR code")
														.setMessage(
																"Please Scan QR code")
														.setCancelable(false)
														.setNegativeButton(
																"Cancel",
																new DialogInterface.OnClickListener() {
																	public void onClick(
																			DialogInterface dialog,
																			int which) {
																	}
																})
														.setNeutralButton(
																"Scan Now",
																new DialogInterface.OnClickListener() {
																	public void onClick(
																			DialogInterface dialog,
																			int whichButton) {
																		flag = 0;
																		Intent myIntent = new Intent(
																				getApplicationContext(),
																				CaptureActivity.class);
																		startActivityForResult(
																				myIntent,
																				0);
																	}
																}).show();

											}
											catch (Exception e) {
												Log.d("main activity: ",
														"Exception in capturing image: "
																+ e.toString());
											}

										}
									})
							.setNegativeButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialoginterface,
												int i) {
											String qr_code = qrcodeFromFile
													.getQrcodeFromFile();
											qrData = qrcodeParse
													.read_qr_data(qr_code);
											if (qrData != null) {
												Log.d(tag,
														"QR code is correct.");
												connectionTime = 0;
												connectWithDevice();
											}
											else {
												Toast.makeText(
														getApplicationContext(),
														"Your saved QR code is not correct, please rescan",
														Toast.LENGTH_LONG)
														.show();
											}
										}
									}).show();
				}
				else {
					Toast.makeText(getBaseContext(),
							"Please keep device on for 5 minutes.",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		disconnectButton = (Button) findViewById(R.id.disconnectButton);
		disconnectButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				disconnectWithDevice();

			}
		});

		// module4Button = (Button) findViewById(R.id.module4Button);
		// module4Button.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// Intent myIntent = new Intent(v.getContext(), Guidance.class);
		// SaveBreathfreqFile.saveBreathFreq(getApplicationContext(), 5);
		// startActivityForResult(myIntent, 0);
		// }
		// });

		if (qrcode != null) {
			qrData = qrcodeParse.read_qr_data(qrcode);
			if (qrData != null) {
				Log.d(tag, "QR code is correct.");
				SaveQRFile.saveQRcode(getApplicationContext(), qrcode);
				connectionTime = 0;
				connectWithDevice();
			}
			else {
				Toast.makeText(getApplicationContext(),
						"Your saved QR code is not correct, please rescan",
						Toast.LENGTH_LONG).show();
			}
		}

	}

	/**
	 * functino to connect with device and show the progress dialog
	 */
	public void connectWithDevice() {
		BTConnectionTask btTask = new BTConnectionTask();

		btTask.execute();
		pd = new ProgressDialog(MainActivity.this);
		pd.setCanceledOnTouchOutside(false);
		pd.setMessage("Connect to" + " (" + Parameter.MAC + ")");
		pd.setIndeterminate(true);
		pd.setCancelable(true);
	}

	/**
	 * function to disconnect with device
	 */
	public void disconnectWithDevice() {
		if (timer != null) {
			timer.cancel();
			biofeedback.stopSound();
			animation.stop();
		}
		bluetooth.disconnectBTSocket();
	}

	public class BTConnectionTask extends AsyncTask<Void, Void, Boolean> {

		protected Boolean doInBackground(Void... x) {
			connectionEstablished = false;
			connectionEstablished = bluetooth.connectBTSocket(Parameter.MAC);
			return connectionEstablished;
		}

		protected void onPostExecute(Boolean result) {
			Log.d(tag, "Value of connectionetablished on postExecute: "
					+ connectionEstablished);
			if (connectionEstablished == true) {
				// initial results_flag as 0
				// metabolismAlgo.results_flag = 0;
				pd.dismiss();
				pd.cancel();
				createTimerTask();
			}
			else {
				connectionTime++;
				pd.dismiss();
				pd.cancel();
				if (connectionTime < 3) {
					connectWithDevice();
				}
				else {
					Log.d(tag, "");
				}
			}
		}
	}

	public void createTimerTask() {

		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			public Handler updateUI = new Handler() {
				@Override
				public void dispatchMessage(Message msg) {
					super.dispatchMessage(msg);
					if (connectionEstablished == true) {
						// Module7: bytes translationd
						boolean thirtyFourBytes_flag = rawData.readBytes();
						if (thirtyFourBytes_flag == true) {
							byte[] bytes = RawData.getInstance()
									.getBufferData();
							boolean data_correct = data.calcUsefulData(bytes);
							if (data_correct == true) {
								index++;
								if (index == 2) {

									// play music and play animation
									img = (ImageView) findViewById(R.id.musicAnim);
									biofeedback = new Biofeedback();
									biofeedback.initSounds(getBaseContext());
									biofeedback.addSound(1, R.raw.testmusic1);
									// createTimerTask();
									guidance.startMusicalGuidance(biofeedback);
									// start animation
									animation = new AnimationDrawable();
									guidance.startAnimation(animation,
											getResources());
									//img.setBackground(animation);
									img.post(new Starter());

									// Module 2: pd check
									pd_check_result = PDCheck
											.check_cartridge(data);
									// set 3 as default, for testing
									pd_check_result = 3;
									// Module 5: adaption reference
									// ref will be stored in
									// adaptionRef.getInstance()
									adaptionRef = adaptionRef.adaptionRefAlgo(
											Parameter.weight, Parameter.height,
											Parameter.gender, Parameter.age);
								}
								else if (index > 2) {

									if (pd_check_result == PDCheck.GOOD_CARTRIDGE) {
										// Module 8: flow calculation
										boolean adaption_cycle_flag = flowCalculation
												.checkCycle(data, index - 2);
										if (adaption_cycle_flag == true) {
											cycle_number++;

											if (cycle_number >= 2) {
												// adaption data will be stored
												// in
												// adaptionData
												FlowCalculation adaptionData = flowCalculation
														.calcRealSuggData();
												// Module 3
												// flowCalculation will equals
												// to
												// null when it is the second
												// cycle,
												// which will be abandoned in
												// adaption suggestion part
												adaptionAlgo.adaptionAlgorithm(
														adaptionData,
														adaptionRef);

											}
										}

										// Module 9
										// check the cycle, when it is one
										// cycle, calculate the cycle's
										// delta data
										delta_data = absorbanceCalc.calcAbsor(
												data, index - 2, delta_data);

										// Check Whether the testing is finished
										if (data.getStatus() == 1) {
											// Module 8
											// calculate final breath frequency
											// and save it into breathfreq.csv
											// also calculate total ve, which
											// will be used in module 11
											FlowCalculation bf_Ve_Data = flowCalculation
													.flowCalcuProcess(
															data,
															delta_data,
															index,
															getApplicationContext());
											// Module 9
											// calculate whole total_delta data,
											// which will be used in module 10
											AbsorbanceCalc total_delta_data = absorbanceCalc
													.calcDeltaData(
															data,
															delta_data,
															getApplicationContext());
											// Module 10
											// calculate con_co2 and con_o2
											// based on the whole total_delta
											// data
											ConcentrationCalc concenData = concentrationCalc
													.calcConcenData(qrData,
															total_delta_data);
											// Module 11
											// calculate ree and rq based on
											// ve (Module8) and concenData
											// (Module10)
											REERQCalc ree_rq_data = reeRqCal
													.calculateREE_RQ(
															concenData,
															bf_Ve_Data,
															getApplicationContext());
											Log.d(tag,
													"REE = "
															+ ree_rq_data
																	.getRee());
											Log.d(tag,
													"RQ = "
															+ ree_rq_data
																	.getRq());
											String currentDateTimeString = DateFormat
													.getDateTimeInstance()
													.format(new Date());
											Log.d(tag, "finish time = "
													+ currentDateTimeString);
											test_finish_time = System
													.currentTimeMillis();
											timer.cancel();
											biofeedback.stopSound();
											animation.stop();
										}
									}
									else {
										timer.cancel();
									}
								}
							}
							else {
								Log.d(tag, "raw data is not correct.");
							}
							Log.d(tag, "readbytes");
						}
						else {
							Log.d(tag, "raw data is not 34 bytes");
						}
					}
				}
			};

			public void run() {
				try {
					updateUI.sendEmptyMessage(0);
				}
				catch (Exception e) {
					Log.d(tag, "caught Exception in run() Handler" + e);
				}
			}

		}, 0, 250); // every 0.25s
	}

	/** To start the feather animation */
	public class Starter implements Runnable {
		public void run() {
			animation.start();
		}
	}
}

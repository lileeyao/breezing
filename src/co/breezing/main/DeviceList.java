//package co.breezing.main;
//
//import java.util.Set;
//import co.breezing.module.R;
//import co.breezing.moduleSix.bluetooth.Bluetooth;
//import android.app.Activity;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.database.Cursor;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
///**
// * Displays a page with the list of Bluetooth devices paired with the phone.
// * User can choose particular device from the list to link it to the app. If the
// * device is not present, it can be scanned and paired from this page.
// */
//public class DeviceList extends Activity {
//
//	private static final String tag = "DeviceList";
//
//	public static String EXTRA_DEVICE_ADDRESS = "device_address";
//	public static String EXTRA_DEVICE_NAME = "device_name";
//
//	/** ArrayAdapter for devices which have already been paired */
//	private ArrayAdapter<String> pairedDevicesArrayAdapter;
//	/** ArrayAdapter for devices new Devices found */
//	private ArrayAdapter<String> newDevicesArrayAdapter;
//
//	public static BluetoothAdapter btAdap = null;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		try {
//			btAdap = Bluetooth.getInstance().getBluetoothAdapter();
//			requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
//			setContentView(R.layout.device_list);
//
//			// Set result CANCELED in case the user backs out
//			setResult(Activity.RESULT_CANCELED);
//
//			// Initialize the button to perform device discovery
//			Button scanButton = (Button) findViewById(R.id.button_scan);
//			scanButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					doDiscovery();
//					v.setVisibility(View.GONE);
//				}
//			});
//
//			// Initialize array adapters. One for already paired devices and
//			// one for newly discovered devices
//			pairedDevicesArrayAdapter = new ArrayAdapter<String>(this,
//					R.layout.device_name);
//			newDevicesArrayAdapter = new ArrayAdapter<String>(this,
//					R.layout.device_name);
//
//			// Find and set up the ListView for paired devices
//			ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
//			pairedListView.setAdapter(pairedDevicesArrayAdapter);
//			pairedListView.setOnItemClickListener(mDeviceClickListener);
//
//			// Find and set up the ListView for newly discovered devices
//			ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
//			newDevicesListView.setAdapter(newDevicesArrayAdapter);
//			newDevicesListView.setOnItemClickListener(mDeviceClickListener);
//
//			// Register for broadcasts when a device is discovered
//			IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
//			this.registerReceiver(mReceiver, filter);
//
//			// Register for broadcasts when discovery has finished
//			filter = new IntentFilter(
//					BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//			this.registerReceiver(mReceiver, filter);
//
//			// Get a set of currently paired devices
//			Set<BluetoothDevice> pairedDevices = btAdap.getBondedDevices();
//
//			// If there are paired devices, add each one to the ArrayAdapter
//			if (pairedDevices.size() > 0) {
//				findViewById(R.id.title_paired_devices).setVisibility(
//						View.VISIBLE);
//				for (BluetoothDevice device : pairedDevices) {
//					pairedDevicesArrayAdapter.add(device.getName() + "\n"
//							+ device.getAddress());
//				}
//			} else {
//				String noDevices = getResources().getText(R.string.none_paired)
//						.toString();
//				pairedDevicesArrayAdapter.add(noDevices);
//			}
//		} catch (Exception e) {
//			Log.d(tag, "Exception in OnCreate Activity in DeviceList " + e);
//		}
//	}
//
//	@Override
//	/** Stop discovery mode and unregister broadcase listeners */
//	protected void onDestroy() {
//		try {
//			super.onDestroy();
//
//			// Make sure we're not doing discovery anymore
//			if (btAdap != null) {
//				btAdap.cancelDiscovery();
//			}
//
//			// Unregister broadcast listeners
//			this.unregisterReceiver(mReceiver);
//		} catch (Exception e) {
//			Log.d(tag, "Exception in onDestroy " + e);
//		}
//	}
//
//	/**
//	 * Start device discover with the BluetoothAdapter
//	 */
//	private void doDiscovery() {
//		Log.d(tag, "doDiscovery()");
//		try {
//
//			setProgressBarIndeterminateVisibility(true);
//			setTitle("Scanning for devices....");
//
//			// Turn on sub-title for new devices
//			findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);
//
//			// If we're already discovering, stop it
//			if (btAdap.isDiscovering()) {
//				btAdap.cancelDiscovery();
//			}
//
//			btAdap.startDiscovery();
//		} catch (Exception e) {
//			Log.d(tag, "Exception in discovering " + e);
//		}
//	}
//
//	/** The on-click listener for all devices in the ListViews */
//	private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
//		public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
//			try {
//				// Cancel discovery because it's costly and we're about to
//				// connect
//				btAdap.cancelDiscovery();
//
//				if (Breezing.device_reference == 0) {
//					info = ((TextView) v).getText().toString();
//					address = info.substring(info.length() - 17);
//					split = null;
//					if (wtinfo == null) {
//						wtinfo = ((TextView) v).getText().toString();
//						wtaddress = wtinfo.substring(wtinfo.length() - 17);
//						wtsplit = null;
//					}
//
//				} else {
//					wtinfo = ((TextView) v).getText().toString();
//					wtaddress = wtinfo.substring(wtinfo.length() - 17);
//					wtsplit = null;
//				}
//
//				// Create the result Intent and include the MAC address
//				Intent intent = new Intent();
//
//				intent.putExtra(EXTRA_DEVICE_ADDRESS, address);
//				intent.putExtra(EXTRA_WTSCALE_ADDRESS, wtaddress);
//
//				try {
//					split = info.split("\\n");
//					wtsplit = wtinfo.split("\\n");
//					intent.putExtra(EXTRA_DEVICE_NAME, split[0]);
//					intent.putExtra(EXTRA_WTSCALE_NAME, wtsplit[0]);
//				} catch (Exception e) {
//					Log.d(tag, "Exception in including BT device name " + e);
//				}
//
//				// Set result and finish this Activity
//				setResult(Activity.RESULT_OK, intent);
//				finish();
//			} catch (Exception e) {
//				Log.d(tag, "Exception in selecting device " + e);
//			}
//		}
//	};
//
//	/**
//	 * The BroadcastReceiver that listens for discovered devices and changes the
//	 * title when discovery is finished
//	 */
//	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			try {
//				String action = intent.getAction();
//
//				// When discovery finds a device
//				if (BluetoothDevice.ACTION_FOUND.equals(action)) {
//					// Get the BluetoothDevice object from the Intent
//					BluetoothDevice device = intent
//							.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//
//					// If it's already paired, skip it, because it's been listed
//					// already
//					if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
//						newDevicesArrayAdapter.add(device.getName() + "\n"
//								+ device.getAddress());
//					}
//					// When discovery is finished, change the Activity title
//				} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
//						.equals(action)) {
//					setProgressBarIndeterminateVisibility(false);
//					setTitle("Select a device to connect");
//					if (newDevicesArrayAdapter.getCount() == 0) {
//						String noDevices = getResources().getText(
//								R.string.none_found).toString();
//						newDevicesArrayAdapter.add(noDevices);
//					}
//
//				}
//			} catch (Exception e) {
//				Log.d(tag, "Exception in onReceive " + e);
//			}
//		}
//	};
//}
package co.breezing.module.four.guidance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import android.os.Environment;
import android.util.Log;

public class BfFromFile {

	private static String breathFreq;
	private static String tag = "BF from file";

	public String getBreathFreq() {
		return breathFreq;
	}

	public void setBreathFreq(String breathFreq) {
		BfFromFile.breathFreq = breathFreq;
	}

	public static String getBfFromFile() {
		File sdcard = Environment.getExternalStorageDirectory();

		// Get the text file
		File file = new File(sdcard + "/BreezingData/", "breathFrequency.csv");

		// Read text from file
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);
			}
			breathFreq = text.toString();
			br.close();
		}
		catch (IOException e) {
			Log.d(tag, "Exception in getting breath freq from file.");
			breathFreq = String.valueOf(15);
		}
		return breathFreq;
	}

}

package co.breezing.module.three.adaptionSuggestion;

import android.util.Log;
import co.breezing.module.eight.flowcalc.FlowCalculation;
import co.breezing.module.five.adaptionReference.AdaptionReference;

/*
 * AdaptionSuggestion.java
 *
 * Input: Tidal Volume, Breathe Frequency and Real-time Ve from
 * Mod-7, and Ref.Tidal Volume, Ref.Breathe Frequency, Ref.Ve
 * from Mod-5. 
 * Output:real time pop-up message to user guidence.
 * 
 * Date: 09/04/2013
 *
 */

public final class AdaptionSuggestion {

	private static String tag = "adaption_algorithm";

	private double bf; // Breathe frequency.
	private double ve; // Real-time Ve.
	private double etv; // Tidal valume.
	// maximum and minimum of corresponding value.
	private double bf_max, bf_min;
	private double ve_max, ve_min;
	private double etv_max, etv_min;

	private static String adaptionSuggestion = null;

	public double getBf() {
		return bf;
	}

	public void setBf(double bf) {
		this.bf = bf;
	}

	public double getVe() {
		return ve;
	}

	public void setVe(double ve) {
		this.ve = ve;
	}

	public double getEtv() {
		return etv;
	}

	public void setEtv(double etv) {
		this.etv = etv;
	}

	public double getBf_max() {
		return bf_max;
	}

	public void setBf_max(double bf_max) {
		this.bf_max = bf_max;
	}

	public double getBf_min() {
		return bf_min;
	}

	public void setBf_min(double bf_min) {
		this.bf_min = bf_min;
	}

	public double getVe_max() {
		return ve_max;
	}

	public void setVe_max(double ve_max) {
		this.ve_max = ve_max;
	}

	public double getVe_min() {
		return ve_min;
	}

	public void setVe_min(double ve_min) {
		this.ve_min = ve_min;
	}

	public double getEtv_max() {
		return etv_max;
	}

	public void setEtv_max(double etv_max) {
		this.etv_max = etv_max;
	}

	public double getEtv_min() {
		return etv_min;
	}

	public void setEtv_min(double etv_min) {
		this.etv_min = etv_min;
	}

	/*
	 * based on the input values, determine the suggestion of user breathing
	 * rhythm. 27 different suggestions in total.
	 */
	public String adaptionAlgorithm(FlowCalculation adaption_data,
			AdaptionReference adaption_ref) {
		// get all the data
		double bf = adaption_data.getFreq_sugg();
		double ve = adaption_data.getVe_sugg();
		double etv = adaption_data.getEtv_sugg();
		double bf_ref_max = adaption_ref.getAdaption_bf_ref_max();
		double bf_ref_min = adaption_ref.getAdaption_bf_ref_min();
		double ve_ref_max = adaption_ref.getAdaption_ve_ref_max();
		double ve_ref_min = adaption_ref.getAdaption_ve_ref_min();
		double etv_ref_max = adaption_ref.getAdaption_etv_ref_max();
		double etv_ref_min = adaption_ref.getAdaption_etv_ref_min();

		// algorithm details:
		if (bf > bf_ref_max) {
			if (ve > ve_ref_max) {
				if (etv > etv_ref_max) {
					adaptionSuggestion = "You’re breathing faster and harder than you would normally. Try taking slower, less deep breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					adaptionSuggestion = "You’re breathing faster than you would normally. Try to breathe more slowly.";
				}
				else if (etv < etv_ref_min) {
					adaptionSuggestion = "You’re breathing faster and not as deeply as you would normally. Try to take slower, deeper breaths.";
				}
			}
			else if (ve <= ve_ref_max && ve >= ve_ref_min) {
				if (etv > etv_ref_max) {
					// impossible
					adaptionSuggestion = "You’re breathing faster and harder than you would normally. Try taking slower, less deep breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					adaptionSuggestion = "You’re breathing faster than you would normally. Try to breathe more slowly.";
				}
				else if (etv < etv_ref_min) {
					adaptionSuggestion = "You’re breathing faster and not as deeply as you would normally. Try to take slower, deeper breaths.";
				}
			}
			else if (ve < ve_ref_min) {
				if (etv > etv_ref_max) {
					// impossible
					adaptionSuggestion = "You’re breathing faster and harder than you would normally. Try taking slower, less deep breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					// impossible
					adaptionSuggestion = "You’re breathing faster than you would normally. Try to breathe more slowly.";
				}
				else if (etv < etv_ref_min) {
					adaptionSuggestion = "You’re breathing faster and not as deeply as you would normally. Try to take slower, deeper breaths.";
				}
			}
		}
		else if (bf <= bf_ref_max && bf >= bf_ref_min) {
			if (ve > ve_ref_max) {
				if (etv > etv_ref_max) {
					adaptionSuggestion = "You’re breathing harder than you would normally. Try taking more shallow breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					// impossible
					adaptionSuggestion = "Try to relax a bit more.";
				}
				else if (etv < etv_ref_min) {
					// impossible
					adaptionSuggestion = "Try to breathe more deeply or check if air is escaping from the nose or mouthpiece.  Use your fingers to clamp your nose as you exhale.";
				}
			}
			else if (ve <= ve_ref_max && ve >= ve_ref_min) {
				if (etv > etv_ref_max) {
					// impossible
					adaptionSuggestion = "You’re breathing harder than you would normally. Try taking more shallow breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					adaptionSuggestion = "You’re doing great!";
				}
				else if (etv < etv_ref_min) {
					adaptionSuggestion = "Try to breathe more deeply or check if air is escaping from the nose or mouthpiece.  Use your fingers to clamp your nose as you exhale.";
				}
			}
			else if (ve < ve_ref_min) {
				if (etv > etv_ref_max) {
					// impossible
					adaptionSuggestion = "You’re breathing harder than you would normally. Try taking more shallow breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					// impossible
					adaptionSuggestion = "Please make sure you’re exhaling into the device.";
				}
				else if (etv < etv_ref_min) {
					adaptionSuggestion = "Try to breathe more deeply or check if air is escaping from the nose or the mouthpiece.  Use your fingers to clamp your nose as you exhale.";
				}
			}
		}
		else if (bf < bf_ref_min) {
			if (ve > ve_ref_max) {
				if (etv > etv_ref_max) {
					adaptionSuggestion = "You’re breathing slower and more deeply than you would normally. Try taking quicker, less deep breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					// impossible
					adaptionSuggestion = "You’re breathing slower than you would normally. Try taking quicker breaths.";
				}
				else if (etv < etv_ref_min) {
					// impossible
					adaptionSuggestion = "You’re breathing slower and not as deeply as you would normally. Try taking quicker, deeper breaths.";
				}
			}
			else if (ve <= ve_ref_max && ve >= ve_ref_min) {
				if (etv > etv_ref_max) {
					adaptionSuggestion = "You’re breathing slower and more deeply than you would normally. Try taking quicker, less deep breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					adaptionSuggestion = "You’re breathing slower than you would normally. Try taking quicker breaths.";
				}
				else if (etv < etv_ref_min) {
					adaptionSuggestion = "You’re breathing slower and not as deeply as you would normally. Try taking quicker, deeper breaths.";
				}
			}
			else if (ve < ve_ref_min) {
				if (etv > etv_ref_max) {
					adaptionSuggestion = "You’re breathing slower and more deeply than you would normally. Try taking quicker, less deep breaths.";
				}
				else if (etv <= etv_ref_max && etv >= etv_ref_min) {
					adaptionSuggestion = "You’re breathing slower than you would normally. Try taking quicker breaths.";
				}
				else if (etv < etv_ref_min) {
					adaptionSuggestion = "You’re breathing slower and not as deeply as you would normally. Try taking quicker, deeper breaths.";
				}
			}
		}
		Log.d(tag, adaptionSuggestion);
		return adaptionSuggestion;
	}
}

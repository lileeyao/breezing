package co.breezing.module.one.qrcode;

import android.util.Log;

public class QRcodeParse {

	private static String tag = "QRcode Parse";

	private double[] var_array = new double[6];
	/**
	 * batch number
	 */
	private String version_number;
	/**
	 * calibration date
	 */
	private String manufacture_date;
	/**
	 * package date and cartridge number
	 */
	private String package_number;
	/**
	 * seems as 01 all the time
	 */
	private String cartridge_number;
	/**
	 * 2733 all the time
	 */
	private String breezing_unique_code;

	public double[] getVar_array() {
		return var_array;
	}

	public void setVar_array(double[] var_array) {
		this.var_array = var_array;
	}

	public String getVersion_number() {
		return version_number;
	}

	public void setVersion_number(String version_number) {
		this.version_number = version_number;
	}

	public String getManufacture_date() {
		return manufacture_date;
	}

	public void setManufacture_date(String manufacture_date) {
		this.manufacture_date = manufacture_date;
	}

	public String getPackage_number() {
		return package_number;
	}

	public void setPackage_number(String package_number) {
		this.package_number = package_number;
	}

	public String getCartridge_number() {
		return cartridge_number;
	}

	public void setCartridge_number(String cartridge_number) {
		this.cartridge_number = cartridge_number;
	}

	public String getBreezing_unique_code() {
		return breezing_unique_code;
	}

	public void setBreezing_unique_code(String breezing_unique_code) {
		this.breezing_unique_code = breezing_unique_code;
	}

	/**
	 * Function to read the coefficient values from QR Code and use them in
	 * calibration equation. QR Code variable description. v1=+XX.YYY v2=+X.YYYY
	 * v3=+X.YYY v4=+XX.YYY v5=+X.YYY v6=+XX.YYY. Concetration o2 = v1 + (v2 *
	 * Test time) +(v3 * total delta absorbance of o2). Concentration co2 = v4 +
	 * (v5 * Test time) + (v6 * normalized total delta absorbance of co2)
	 */
	// public boolean read_qr_data(String qr_code) {
	// try {
	// // check whether qr_code has 70 characters
	// if (qr_code.length() == 70) {
	// double[] qc = new double[qr_code.length()];
	// for (int i = 0; i < qr_code.length(); i++) {
	// qc[i] = Double.parseDouble(Character.toString(qr_code
	// .charAt(i)));
	// }
	// breezing_unique_code = qr_code.substring(66, 70);
	//
	// // check whether unique code is 2733
	// if (breezing_unique_code.equals("2733")) {
	//
	// version_number = qr_code.substring(34, 38);
	// manufacture_date = qr_code.substring(38, 46);
	// package_number = qr_code.substring(46, 64);
	// cartridge_number = qr_code.substring(64, 66);
	//
	// var1 = RoundData
	// .roundThreeDecimals((1 - qc[0] * (2))
	// * (qc[1] * 10 + qc[2] + qc[3] * 0.1 + qc[4]
	// * 0.01 + qc[5] * 0.001));
	// var2 = RoundData.roundFourDecimals((1 - qc[6] * (2))
	// * (qc[7] + qc[8] * 0.1 + qc[9] * 0.01 + qc[10]
	// * 0.001 + qc[11] * 0.0001));
	// var3 = RoundData
	// .roundThreeDecimals((1 - qc[12] * (2))
	// * (qc[13] + qc[14] * 0.1 + qc[15] * 0.01 + qc[16] * 0.001));
	// var4 = RoundData.roundThreeDecimals((1 - qc[17] * (2))
	// * (qc[18] * 10 + qc[19] + qc[20] * 0.1 + qc[21]
	// * 0.01 + qc[22] * 0.001));
	// var5 = RoundData
	// .roundThreeDecimals((1 - qc[23] * (2))
	// * (qc[24] + qc[25] * 0.1 + qc[26] * 0.01 + qc[27] * 0.001));
	// var6 = RoundData.roundThreeDecimals((1 - qc[28] * (2))
	// * (qc[29] * 10 + qc[30] + qc[31] * 0.1 + qc[32]
	// * 0.01 + qc[33] * 0.001));
	//
	// Log.d(tag, "var1~6: " + var1 + " " + var2 + " " + var3
	// + " " + var4 + " " + var5 + " " + var6 + " ");
	//
	// Log.d(tag, "version_number = " + version_number
	// + "\n manufacture_date = " + manufacture_date
	// + "\n package_number =  " + package_number
	// + "\n cartridge_number = " + cartridge_number);
	// return true;
	// }
	// else {
	// Log.d(tag, "qr code is not correct, uniq code is not 2733");
	// return false;
	// }
	// }
	// else {
	// Log.d(tag,
	// "qr code is not correct, lenth of qr code is not correct.");
	// return false;
	// }
	//
	// }
	// catch (Exception e) {
	// Log.d(tag, "Exception in parse qr code: " + e.toString());
	// return false;
	// }
	// }

	public QRcodeParse read_qr_data(String qr_code) {
		QRcodeParse qrData = new QRcodeParse();
		try {
			// check whether qr_code has 90 characters
			if (qr_code.length() == 90) {
				double[] qc = new double[qr_code.length()];
				for (int i = 0; i < qr_code.length(); i++) {
					qc[i] = Double.parseDouble(Character.toString(qr_code
							.charAt(i)));
				}
				breezing_unique_code = qr_code.substring(86, 90);

				// check whether unique code is 2733
				if (breezing_unique_code.equals("2733")) {

					version_number = qr_code.substring(54, 58);
					manufacture_date = qr_code.substring(58, 66);
					package_number = qr_code.substring(66, 84);
					cartridge_number = qr_code.substring(84, 86);

					for (int i = 0, j = 0; i <= 5; i++, j += 9) {
						var_array[i] = (1 - qc[j] * (2))
								* (qc[j + 1] * 1 + qc[j + 2] * 0.1 + qc[j + 3]
										* 0.01 + qc[j + 4] * 0.001 + qc[j + 5]
										* 0.0001 + qc[j + 6] * 0.00001)
								* Math.pow(10, (1 - qc[j + 7] * (2))
										* qc[j + 8]);
					}

					Log.d(tag, "var1~6: " + var_array[0] + " " + var_array[1]
							+ " " + var_array[2] + " " + var_array[3] + " "
							+ var_array[4] + " " + var_array[5] + " ");

					Log.d(tag, "version_number = " + version_number
							+ "\n manufacture_date = " + manufacture_date
							+ "\n package_number =  " + package_number
							+ "\n cartridge_number = " + cartridge_number);
					
					qrData.setVar_array(var_array);
					qrData.setBreezing_unique_code(breezing_unique_code);
					qrData.setCartridge_number(cartridge_number);
					qrData.setManufacture_date(manufacture_date);
					qrData.setPackage_number(package_number);
					qrData.setVersion_number(version_number);
					return qrData;
				}
				else {
					Log.d(tag, "qr code is not correct, uniq code is not 2733");
					return null;
				}
			}
			else {
				Log.d(tag,
						"qr code is not correct, lenth of qr code is not correct.");
				return null;
			}

		}
		catch (Exception e) {
			Log.d(tag, "Exception in parse qr code: " + e.toString());
			return null;
		}
	}

}

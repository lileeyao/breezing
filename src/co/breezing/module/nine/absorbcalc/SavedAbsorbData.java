package co.breezing.module.nine.absorbcalc;

public class SavedAbsorbData {

	private int breathSignal;
	private double absorbance_O2;
	private double absorbance_CO2;
	private int dataPoints;
	private double time;
	private double abs_o2_int;
	private double abs_o2_final;
	private double abs_co2_int;
	private double abs_co2_final;

	private double total_delta_o2;
	private double total_delta_co2;
	private double conc_o2;
	private double conc_co2;

	private double ve;
	private double ree;
	private double rq;

	public int getBreathSignal() {
		return breathSignal;
	}

	public void setBreathSignal(int breathSignal) {
		this.breathSignal = breathSignal;
	}

	public double getAbsorbance_O2() {
		return absorbance_O2;
	}

	public void setAbsorbance_O2(double absorbance_O2) {
		this.absorbance_O2 = absorbance_O2;
	}

	public double getAbsorbance_CO2() {
		return absorbance_CO2;
	}

	public void setAbsorbance_CO2(double absorbance_CO2) {
		this.absorbance_CO2 = absorbance_CO2;
	}

	public int getDataPoints() {
		return dataPoints;
	}

	public void setDataPoints(int dataPoints) {
		this.dataPoints = dataPoints;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public double getAbs_o2_int() {
		return abs_o2_int;
	}

	public void setAbs_o2_int(double abs_o2_int) {
		this.abs_o2_int = abs_o2_int;
	}

	public double getAbs_o2_final() {
		return abs_o2_final;
	}

	public void setAbs_o2_final(double abs_o2_final) {
		this.abs_o2_final = abs_o2_final;
	}

	public double getAbs_co2_int() {
		return abs_co2_int;
	}

	public void setAbs_co2_int(double abs_co2_int) {
		this.abs_co2_int = abs_co2_int;
	}

	public double getAbs_co2_final() {
		return abs_co2_final;
	}

	public void setAbs_co2_final(double abs_co2_final) {
		this.abs_co2_final = abs_co2_final;
	}

	public double getTotal_delta_o2() {
		return total_delta_o2;
	}

	public void setTotal_delta_o2(double total_delta_o2) {
		this.total_delta_o2 = total_delta_o2;
	}

	public double getTotal_delta_co2() {
		return total_delta_co2;
	}

	public void setTotal_delta_co2(double total_delta_co2) {
		this.total_delta_co2 = total_delta_co2;
	}

	public double getConc_o2() {
		return conc_o2;
	}

	public void setConc_o2(double conc_o2) {
		this.conc_o2 = conc_o2;
	}

	public double getConc_co2() {
		return conc_co2;
	}

	public void setConc_co2(double conc_co2) {
		this.conc_co2 = conc_co2;
	}

	public double getVe() {
		return ve;
	}

	public void setVe(double ve) {
		this.ve = ve;
	}

	public double getRee() {
		return ree;
	}

	public void setRee(double ree) {
		this.ree = ree;
	}

	public double getRq() {
		return rq;
	}

	public void setRq(double rq) {
		this.rq = rq;
	}
}
package co.breezing.module.eight.flowcalc;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import android.content.Context;
import android.util.Log;
import co.breezing.metabolism.parameter.Constant;
import co.breezing.module.nine.absorbcalc.AbsorbanceCalc;
import co.breezing.module.nine.absorbcalc.SavePressureData;
import co.breezing.module.nine.absorbcalc.SavedRawData;
import co.breezing.module.seven.dataTranslation.DataTranslation;

public class FlowCalculation {

	private static String tag = "flowCalculation";

	private ArrayList<Integer> cycle_index_initial = new ArrayList<Integer>();
	private ArrayList<Integer> cycle_index_final = new ArrayList<Integer>();
	private ArrayList<Double> cycle_volume_initial = new ArrayList<Double>();
	private ArrayList<Double> cycle_volume_final = new ArrayList<Double>();
	private ArrayList<Double> etv = new ArrayList<Double>();
	private ArrayList<Double> etv_avg = new ArrayList<Double>();
	private ArrayList<Double> freq = new ArrayList<Double>();
	private ArrayList<Double> freq_avg = new ArrayList<Double>();
	private ArrayList<Double> ve_adaption = new ArrayList<Double>();
	private ArrayList<Double> ve_adaption_avg = new ArrayList<Double>();

	private ArrayList<Double> volume_list = new ArrayList<Double>();// used to
																	// check
																	// whether
																	// there is
																	// a cycle.

	private double etv_sugg; // used in module 3 to give suggestion
	private double freq_sugg;// used in module 3 to give suggestion
	private double ve_sugg;// used in module 3 to give suggestion

	private ArrayList<Double> adaption_time = new ArrayList<Double>();
	private boolean increasing_flag = false; // true means increasing, false
												// means flat
	private boolean detectStatus_flag = false; // when status equals to 1,
												// change to true

	private int cycle_index_i = 0;// i value for cycle, means how many cycles,
									// if status ==1, this is the cycle number
	private int cycle_index_j = 0;// j value for cycle, means in one cycles how
									// many data there

	private ArrayList<Double> thermistor_list = new ArrayList<Double>();
	private int start_index; // index to show volume increase
	private int fin_index; // status becomes 1 on fin_index
	private double testTime; // the time of whole test
	private int frequency;
	private double ve;

	public double getEtv_sugg() {
		return etv_sugg;
	}

	public void setEtv_sugg(double etv_sugg) {
		this.etv_sugg = etv_sugg;
	}

	public double getFreq_sugg() {
		return freq_sugg;
	}

	public void setFreq_sugg(double freq_sugg) {
		this.freq_sugg = freq_sugg;
	}

	public double getVe_sugg() {
		return ve_sugg;
	}

	public void setVe_sugg(double ve_sugg) {
		this.ve_sugg = ve_sugg;
	}

	public ArrayList<Double> getEtv_avg() {
		return etv_avg;
	}

	public void setEtv_avg(ArrayList<Double> etv_avg) {
		this.etv_avg = etv_avg;
	}

	public ArrayList<Double> getFreq_avg() {
		return freq_avg;
	}

	public void setFreq_avg(ArrayList<Double> freq_avg) {
		this.freq_avg = freq_avg;
	}

	public ArrayList<Double> getVe_adaption_avg() {
		return ve_adaption_avg;
	}

	public void setVe_adaption_avg(ArrayList<Double> ve_adaption_avg) {
		this.ve_adaption_avg = ve_adaption_avg;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public double getVe() {
		return ve;
	}

	public void setVe(double ve) {
		this.ve = ve;
	}

	/**
	 * 
	 * Function to check whether there is already one cycle. In every 0.25s,
	 * check whether this point is the end of cycle. One cycle only means breath
	 * in then breathe out.
	 * 
	 * @param volume
	 * @param index
	 * @return find one cycle (breathe in then breathe out) return true;
	 *         otherwise, return false
	 */
	public boolean checkCycle(DataTranslation data, int index) {
		double volume = data.getVolume();
		// add data into arraylist
		volume_list.add(volume);
		Log.d(tag, String.valueOf(volume));

		// initial whetherCycle as false
		boolean whetherCycle = false;

		// check cycle information
		if (index >= 3) {
			// change double to integer so that it can be equal, otherwise
			if (Math.round(volume_list.get(index - 1)) == Math
					.round(volume_list.get(index - 2))) {
				if (increasing_flag == true) {
					// this condition means increasing volume starts to be flat
					if (cycle_index_j < 4) {
						// ignore the increasing part as one cycle when the plot
						// number is less than 4
						cycle_index_j = 0;
						increasing_flag = false;
					}
					else {
						cycle_index_i++;
						cycle_index_j = 0;
						increasing_flag = false;
						// new adaption algorithm part
						if (cycle_index_i == 1) {
							// in the first cycle, we don't know when it starts,
							// we
							// ignore it. consider the second cycle as [0],
							// set the initial value for cycle_index_initial[0]
							// and
							// cycle_volume_initial[0]
							cycle_index_initial.add(cycle_index_i - 1,
									index - 1);
							cycle_volume_initial.add(cycle_index_i - 1,
									volume_list.get(index - 1));
						}
						else {
							// from the third cycle, we can directly get last
							// cycle's final value and this new cycle's initial
							// value
							cycle_volume_final.add(cycle_index_i - 2,
									volume_list.get(index - 1));
							cycle_index_final.add(cycle_index_i - 2, index - 1);
							cycle_volume_initial.add(cycle_index_i - 1,
									volume_list.get(index - 1));
							cycle_index_initial.add(cycle_index_i - 1,
									index - 1);

							Log.d("adaption:volume", "final:"
									+ cycle_volume_final.get(cycle_index_i - 2));
							Log.d("adaption:volume",
									"initial:"
											+ cycle_volume_initial
													.get(cycle_index_i - 2));
							whetherCycle = true;
						}
					}
				}
			}
			else {
				cycle_index_j++;
				increasing_flag = true;
			}
		}
		return whetherCycle;
	}

	/**
	 * This function is used to calculate adaption data to see adaption
	 * suggestion.
	 */
	public FlowCalculation calcRealSuggData() {
		FlowCalculation adaptionData = new FlowCalculation();
		if (cycle_index_i >= 3) {
			etv.add(cycle_index_i - 3,
					(cycle_volume_final.get(cycle_index_i - 3) - cycle_volume_initial
							.get(cycle_index_i - 3))
							* 6000
							/ (Constant.volume_calibration * 100));
			etv.add(cycle_index_i - 2,
					(cycle_volume_final.get(cycle_index_i - 2) - cycle_volume_initial
							.get(cycle_index_i - 2))
							* 6000
							/ (Constant.volume_calibration * 100));
			etv_avg.add(
					cycle_index_i - 3,
					(etv.get(cycle_index_i - 3) + etv.get(cycle_index_i - 2)) / 2);
			adaption_time
					.add(cycle_index_i - 3, (cycle_index_final
							.get(cycle_index_i - 3) - cycle_index_initial
							.get(cycle_index_i - 3)) * 0.25);
			adaption_time
					.add(cycle_index_i - 2, (cycle_index_final
							.get(cycle_index_i - 2) - cycle_index_initial
							.get(cycle_index_i - 2)) * 0.25);
			freq.add(cycle_index_i - 3,
					60 / adaption_time.get(cycle_index_i - 3));
			freq.add(cycle_index_i - 2,
					60 / adaption_time.get(cycle_index_i - 2));
			freq_avg.add(cycle_index_i - 3, 120 / (adaption_time
					.get(cycle_index_i - 3) + adaption_time
					.get(cycle_index_i - 2)));
			ve_adaption.add(cycle_index_i - 3, etv.get(cycle_index_i - 3)
					* freq.get(cycle_index_i - 3));
			ve_adaption.add(cycle_index_i - 2, etv.get(cycle_index_i - 2)
					* freq.get(cycle_index_i - 2));
			ve_adaption_avg.add(cycle_index_i - 3, (ve_adaption
					.get(cycle_index_i - 3) + ve_adaption
					.get(cycle_index_i - 2)) / 2);

			etv_sugg = etv_avg.get(cycle_index_i - 3);
			freq_sugg = freq_avg.get(cycle_index_i - 3);
			ve_sugg = ve_adaption_avg.get(cycle_index_i - 3);

			Log.d("adaption", "index: " + String.valueOf(cycle_index_i - 3));
			Log.d("adaption", "etv: " + String.valueOf(etv_sugg));
			Log.d("adaption", "freq: " + String.valueOf(freq_sugg));
			Log.d("adaption", "ve: " + String.valueOf(ve_sugg));

			// put all calculate adaption data into instance.
			// sugg is used to give real suggestion in every cycle
			// avg is used to give final conclusion in the end of test
			adaptionData.setEtv_sugg(etv_sugg);
			adaptionData.setFreq_sugg(freq_sugg);
			adaptionData.setVe_sugg(ve_sugg);
			adaptionData.setVe_adaption_avg(ve_adaption_avg);
			adaptionData.setFreq_avg(freq_avg);
			adaptionData.setEtv_avg(etv_avg);

			// based on the average
			// value,
			// use adaptionAlgorithm
			// function to give
			// suggestions
			// adaptionReference.adaptionRefAlgo(Parameter.weight,
			// Parameter.height, Parameter.gender, Parameter.age);
			// adaptionSuggestion = adaptionAlgorithm.adaptionAlgorithm(
			// freq_avg.get(cycle_index_i - 3),
			// ve_adaption_avg.get(cycle_index_i - 3),
			// etv_avg.get(cycle_index_i - 3),
			// adaptionReference.getAdaption_bf_ref_max(),
			// adaptionReference.getAdaption_bf_ref_min(),
			// adaptionReference.getAdaption_ve_ref_max(),
			// adaptionReference.getAdaption_ve_ref_min(),
			// adaptionReference.getAdaption_etv_ref_max(),
			// adaptionReference.getAdaption_etv_ref_min());
		}
		return adaptionData;
	}

	/**
	 * This function is used to calculate ve, breath frequency, and transfer
	 * pressure It will store all the thermistor data then get the minmum data
	 * of thermistor. Based on this minmum thermistor data, calculate factor
	 * Based on the index when status data changes, it will calculate test_time
	 * By using test_time and factor, we can calculate ve. Based on cycle number
	 * and testTime, we can calculate breath frequency, then save it into the BF
	 * file.
	 */
	public FlowCalculation flowCalcuProcess(DataTranslation data,
			AbsorbanceCalc delta_data, int index, Context ctx) {

		FlowCalculation bf_Ve_Data = new FlowCalculation();

		// add data into arraylist
		double thermistor = data.getThermistor();
		double pressure = data.getPressure();
		double status = data.getStatus();

		thermistor_list.add(thermistor);

		if (status == 1) {
			if (detectStatus_flag == false) {
				start_index = delta_data.getIndex_initial();
				fin_index = index;
				testTime = (fin_index - start_index) * 0.25;
				detectStatus_flag = true;
				Log.d(tag, "testTime = " + testTime);
			}
			double thermistor_min = Collections.min(thermistor_list);
			double factor = getActualVolumeFactor(thermistor_min);
			ve = (6000 * factor * 60) / testTime;
			Log.d(tag, "ve = " + ve);
			frequency = (int) Math.round(Double.valueOf(cycle_index_i)
					/ (testTime / 60));
			Log.d(tag, "frequency = " + frequency);
			if (frequency < 5) {
				frequency = 5;
			}
			else if (frequency > 20) {
				frequency = 20;
			}
			SaveBreathfreqFile.saveBreathFreq(ctx, frequency);
		}
		bf_Ve_Data.setFrequency(frequency);
		bf_Ve_Data.setVe(ve);

		/**************csv file******************/
		// write the raw data csv file
		SavePressureData.saveData(ctx, delta_data.getSavedRawData_list());
		/********************************/
		return bf_Ve_Data;
	}

	/**
	 * Function to compute the correction factor based on temperature and water
	 * vapor pressure. Presure list shows the list of pressures for temperatures
	 * 10 to 39.8 with an interval of 0.2. Takes input as the minimum value of
	 * thermistor during the test. It is used to calculate the maximum value of
	 * temperature during the test. max temperature = (1.0 / ((1.0 / (273.0 +
	 * 25.0)) + (1.0 / 3380.0) log10(min_thermistor / 0.0001 / 10000))) - 273.0.
	 * If temperature is less than 10 or greater than 39.8, set them to boundary
	 * values. Correction factor is computed by the equation factor = ((740.0 -
	 * pressure) * 273.0) / (760.0 * (max_temperature + 273.0));
	 */
	private double getActualVolumeFactor(double min_thermistor) {
		try {
			double pressure_list[] = { 9.209, 9.333, 9.458, 9.585, 9.714,
					9.844, 9.976, 10.109, 10.244, 10.380, 10.518, 10.658,
					10.799, 10.941, 11.085, 11.231, 11.379, 11.528, 11.680,
					11.833, 11.987, 12.144, 12.302, 12.462, 12.624, 12.788,
					12.953, 13.121, 13.290, 13.461, 13.634, 13.809, 13.987,
					14.166, 14.347, 14.530, 14.715, 14.903, 15.092, 15.284,
					15.477, 15.673, 15.871, 16.071, 16.272, 16.477, 16.685,
					16.894, 17.105, 17.319, 17.535, 17.753, 17.974, 18.197,
					18.422, 18.650, 18.880, 19.113, 19.349, 19.587, 19.827,
					20.070, 20.316, 20.565, 20.815, 21.068, 21.324, 21.583,
					21.845, 22.110, 22.377, 22.648, 22.922, 23.198, 23.477,
					23.756, 24.039, 24.326, 24.617, 24.912, 25.209, 25.509,
					25.812, 26.117, 26.426, 26.739, 27.055, 27.374, 27.696,
					28.021, 28.349, 28.680, 29.015, 29.354, 29.697, 30.043,
					30.392, 30.745, 31.102, 31.461, 31.824, 32.191, 32.561,
					32.934, 33.312, 33.694, 34.082, 34.471, 34.864, 35.261,
					35.663, 36.068, 36.477, 36.891, 37.308, 37.729, 38.155,
					38.584, 39.018, 39.457, 39.898, 40.344, 40.796, 41.251,
					41.710, 42.175, 42.644, 43.117, 43.595, 44.078, 44.563,
					45.054, 45.549, 46.050, 46.556, 47.067, 47.582, 48.102,
					48.627, 49.157, 49.692, 50.231, 50.774, 51.323, 51.879,
					52.442, 53.009, 53.580, 54.156, 54.737 };

			double max_temperature;
			double pressure = 0.872;
			double factor;
			Log.d(tag, "Gav max thermistor: " + min_thermistor);
			max_temperature = (1.0 / ((1.0 / (273.0 + 25.0)) + (1.0 / 3380.0)
					* Math.log(min_thermistor / 0.0001 / 10000))) - 273.0;
			// max_temperature = 25.0; //Testing purpose
			Log.d(tag, "GAV max temp: " + max_temperature);
			DecimalFormat oneDigit = new DecimalFormat("#,##0.0");
			String s = oneDigit.format(max_temperature);
			max_temperature = Double.parseDouble(s);

			if (max_temperature < 10) {
				max_temperature = 10;
			}
			if (max_temperature > 39.8) {
				max_temperature = 39.8;
			}
			if (max_temperature >= 10 && max_temperature <= 39.8) {
				if ((max_temperature * 10) % 2 == 0)// check if even
				{
					pressure = pressure_list[(int) ((max_temperature * 10) - 100) / 2];
				}
				else {
					pressure = (pressure_list[(int) ((max_temperature * 10) - 101) / 2] + pressure_list[(int) ((max_temperature * 10) - 99) / 2]) / 2;
				}
			}

			else {
				Log.d(tag, "Breath temperature out of range: "
						+ max_temperature);
			}
			Log.d(tag, "GAV max temp one decimal: " + max_temperature);
			Log.d(tag, "GAV pressure: " + pressure);
			factor = ((740.0 - pressure) * 273.0)
					/ (760.0 * (max_temperature + 273.0));
			return factor;
		}
		catch (Exception e) {
			Log.d(tag, "Exception in computing temperature pressure factor: "
					+ e.toString());
			return 0.872;
		}
	}
}

package co.breezing.module.two.pdcheck;

import co.breezing.module.seven.dataTranslation.DataTranslation;
import android.util.Log;

public class PDCheck {

	public static String tag = "PDcheck";

	public static int NO_CARTRIDGE = 0;
	public static int USED_CARTRIDGE = 1;
	public static int FLIPPED_CARTRIDGE = 2;
	public static int GOOD_CARTRIDGE = 3;

	private static int cartridge_status;

	public int getCartridge_status() {
		return cartridge_status;
	}

	public void setCartridge_status(int cartridge_status) {
		PDCheck.cartridge_status = cartridge_status;
	}

	/**
	 * Test if the cartridge under use is fresh or not based on the photodiode
	 * values.
	 */
	public static int check_cartridge(DataTranslation data) {
		double pd1 = data.getPd1();
		double pd2 = data.getPd2();
		double pd3 = data.getPd3();
		double pd4 = data.getPd4();
		Log.d(tag, "Cartridge values: " + pd1 + " " + pd2 + " " + pd3 + " "
				+ pd4);
		if (pd1 > 1.1 && pd2 > 1.1 && pd3 > 1.1 && pd4 > 1.1) {
			cartridge_status = NO_CARTRIDGE;
			// displayMessage = no_cartriage_str;
			Log.d(tag,
					"Please insert your Breezing cartridge into the device and reset your device.");
		}
		else if (pd1 >= 0.75 && pd1 <= 1.25 && pd2 >= 0.35 && pd2 <= 1
				&& pd3 >= 0.5 && pd3 <= 1.4 && pd4 >= 0.75 && pd4 <= 1.25) {
			cartridge_status = GOOD_CARTRIDGE;
			// freshCartridge = true;// set flag of fresh cartridge, in order to
			// // display instruction.
			Log.d(tag, "Good Cartridge.");
		}
		else if (pd1 >= 0.3 && pd1 <= 0.6 && pd2 >= 0.7 && pd2 <= 1.0
				&& pd3 >= 0.55 && pd3 <= 0.85 && pd4 >= 0.7 && pd4 <= 1.0) {
			cartridge_status = FLIPPED_CARTRIDGE;
			// displayMessage = flip_cartriage_str;
			Log.d(tag,
					"Please reinsert the cartridge with the yellow square at the bottom right-hand corner and reset your device.");
		}
		else {
			cartridge_status = USED_CARTRIDGE;
			// displayMessage = used_cartriage_str;
			Log.d(tag,
					"Please insert a fresh Breezing cartridge to take a new test and reset your device.");
		}
		return cartridge_status;
	}
}

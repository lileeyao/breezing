package co.breezing.module.ten.concentration;

import android.util.Log;
import co.breezing.module.nine.absorbcalc.AbsorbanceCalc;
import co.breezing.module.nine.absorbcalc.SavedAbsorbData;
import co.breezing.module.one.qrcode.QRcodeParse;

/*
 * ConcentrationCalc.java
 *
 * Simple equations for CO2 & O2 concentration.
 *
 * Input : qrData, total_delta_data
 * Output: O2 concentration, CO2 concentration
 *
 */

public class ConcentrationCalc {

	private static String tag = "Module 10: concentration data calculation";

	private double concen_o2;
	private double concen_co2;
	private SavedAbsorbData savedAbsorbData;

	public double getConcen_o2() {
		return concen_o2;
	}

	public void setConcen_o2(double concen_o2) {
		this.concen_o2 = concen_o2;
	}

	public double getConcen_co2() {
		return concen_co2;
	}

	public void setConcen_co2(double concen_co2) {
		this.concen_co2 = concen_co2;
	}

	public SavedAbsorbData getSavedAbsorbData() {
		return savedAbsorbData;
	}

	public void setSavedAbsorbData(SavedAbsorbData savedAbsorbData) {
		this.savedAbsorbData = savedAbsorbData;
	}

	/**
	 * calculate concentrate_o2 and concentrate_co2
	 * 
	 * @param qrData
	 * @param total_delta_data
	 * @return
	 */
	public ConcentrationCalc calcConcenData(QRcodeParse qrData,
			AbsorbanceCalc total_delta_data) {

		ConcentrationCalc concenData = new ConcentrationCalc();

		double[] var_array = qrData.getVar_array();
		double total_delta_time = total_delta_data.getTotal_delta_time();
		double total_delta_o2 = total_delta_data.getTotal_delta_absorbance_o2();
		double total_delta_co2 = total_delta_data
				.getTotal_delta_absorbance_co2();

		// for testing
		var_array[0] = 17.673;
		var_array[1] = -0.0256;
		var_array[2] = 3.256;
		var_array[3] = 2.537;
		var_array[4] = 0.004;
		var_array[5] = -2.147;

		double concen_o2 = var_array[0] + (var_array[1] * total_delta_time)
				+ (var_array[2] * total_delta_o2);
		double concen_co2 = var_array[3] + (var_array[4] * total_delta_time)
				+ (var_array[5] * total_delta_co2);
		Log.d(tag, "concen_o2 = " + concen_o2);
		Log.d(tag, "concen_co2 = " + concen_co2);
		concenData.setConcen_co2(concen_co2);
		concenData.setConcen_o2(concen_o2);
		
		savedAbsorbData = new SavedAbsorbData();
		// save data into savedAbsorbData
		savedAbsorbData.setTotal_delta_o2(total_delta_o2);
		savedAbsorbData.setTotal_delta_co2(total_delta_co2);
		savedAbsorbData.setConc_co2(concen_co2);
		savedAbsorbData.setConc_o2(concen_o2);
		concenData.setSavedAbsorbData(savedAbsorbData);
		
		return concenData;
	}

}

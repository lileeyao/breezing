package co.breezing.module.eleven.reerq;

import android.content.Context;
import co.breezing.math.round.RoundData;
import co.breezing.module.eight.flowcalc.FlowCalculation;
import co.breezing.module.nine.absorbcalc.SavedAbsorbData;
import co.breezing.module.ten.concentration.ConcentrationCalc;

/*
 * REERQCalc.java
 *
 * simple equations for REE & RQ calculation.
 *
 * Input : conc_o2, conc_co2
 * Output: REE, RQ
 *
 */

public final class REERQCalc {

	private double ree;
	private double rq;

	private SavedAbsorbData savedAbsorbData;

	public double getRee() {
		return ree;
	}

	public void setRee(double ree) {
		this.ree = ree;
	}

	public double getRq() {
		return rq;
	}

	public void setRq(double rq) {
		this.rq = rq;
	}

	public SavedAbsorbData getSavedAbsorbData() {
		return savedAbsorbData;
	}

	public void setSavedAbsorbData(SavedAbsorbData savedAbsorbData) {
		this.savedAbsorbData = savedAbsorbData;
	}

	public REERQCalc calculateREE_RQ(ConcentrationCalc concenData,
			FlowCalculation veData, Context ctx) {

		REERQCalc ree_rq_data = new REERQCalc();

		double conc_o2 = concenData.getConcen_o2();
		double conc_co2 = concenData.getConcen_co2();
		double ve = veData.getVe();

		double vo2 = (20.93 - conc_o2) * ve * 0.01;
		double vco2 = (conc_co2 - 0.03) * ve * 0.01;

		ree = RoundData.roundTwoDecimals(((3.9 * vo2) + (1.1 * vco2)) * 1.44);
		if (vo2 != 0) {
			rq = RoundData.roundTwoDecimals(vco2 / vo2);
		}

		// Emergency algorithm start
		double vo2_ref = (0.2094 - 0.166) * ve;
		double vco2_ref = (0.037 - 0.0003) * ve;
		double ree_ref = (3.9 * vo2_ref + 1.1 * vco2_ref) * 1.44;
		// 70% is the correction factor now
		if (Math.abs(ree_ref - ree) > ree_ref * 0.70) {
			ree = ree_ref;
			rq = 0.85;
		}
		// Emergency algorithm end

		ree_rq_data.setRee(ree);
		ree_rq_data.setRq(rq);

		savedAbsorbData = concenData.getSavedAbsorbData();
		savedAbsorbData.setVe(ve);
		savedAbsorbData.setRee(ree);
		savedAbsorbData.setRq(rq);
		
		ree_rq_data.setSavedAbsorbData(savedAbsorbData);
		
		/***********************csv file***********************/
		SaveFinalData.saveFinalData(ctx, savedAbsorbData);
		/***********************csv file***********************/

		return ree_rq_data;
	}

}

package co.breezing.module.seven.dataTranslation;

import java.util.ArrayList;
import android.util.Log;
import co.breezing.math.round.RoundData;

public class DataTranslation {

	private static String tag = "rawData calculation";

	private static DataTranslation data = new DataTranslation();

	// 34 bytes:
	// Photodiode 1-4 (4 bytes each), Status (2 bytes), Resolution (4 bytes),
	// Pressure (4 bytes), Volume (4 bytes), Temperature (4 bytes).
	// pd1~4 and temperature need calibration.
	/**
	 * photodiode1, calculate by first 4 bytes sensor data and resolution
	 */
	private double pd1;
	/**
	 * photodiode2, calculate by second 4 bytes sensor data and resolution
	 */
	private double pd2;
	/**
	 * photodiode3, calculate by third 4 bytes sensor data and resolution
	 */
	private double pd3;
	/**
	 * photodiode4, calculate by fourth 4 bytes sensor data and resolution
	 */
	private double pd4;
	/**
	 * status, 2 bytes, 0 means not finished, 1 means finished
	 */
	private double status = 0;
	/**
	 * resolution, 4 bytes used to calculate pd1~pd4 and thermistor
	 */
	private double resolution = 0;
	/**
	 * pressure, 4 bytes
	 */
	private double pressure = 0;
	/**
	 * thermistor, calculate by 4 bytes temperature data and resolution
	 */
	private double thermistor = 0;
	/**
	 * volume, 4 bytes
	 */
	private double volume = 0;

	private ArrayList<Double> volume_list = new ArrayList<Double>();
	private ArrayList<Double> pd1_list = new ArrayList<Double>();
	private ArrayList<Double> pd2_list = new ArrayList<Double>();
	private ArrayList<Double> pd3_list = new ArrayList<Double>();
	private ArrayList<Double> pd4_list = new ArrayList<Double>();

	public double getPd1() {
		return pd1;
	}

	public void setPd1(double pd1) {
		this.pd1 = pd1;
	}

	public double getPd2() {
		return pd2;
	}

	public void setPd2(double pd2) {
		this.pd2 = pd2;
	}

	public double getPd3() {
		return pd3;
	}

	public void setPd3(double pd3) {
		this.pd3 = pd3;
	}

	public double getPd4() {
		return pd4;
	}

	public void setPd4(double pd4) {
		this.pd4 = pd4;
	}

	public double getStatus() {
		return status;
	}

	public void setStatus(double status) {
		this.status = status;
	}

	public double getResolution() {
		return resolution;
	}

	public void setResolution(double resolution) {
		this.resolution = resolution;
	}

	public double getPressure() {
		return pressure;
	}

	public void setPressure(double pressure) {
		this.pressure = pressure;
	}

	public double getThermistor() {
		return thermistor;
	}

	public void setThermistor(double thermistor) {
		this.thermistor = thermistor;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public ArrayList<Double> getVolume_list() {
		return volume_list;
	}

	public void setVolume_list(ArrayList<Double> volume_list) {
		this.volume_list = volume_list;
	}

	public ArrayList<Double> getPd1_list() {
		return pd1_list;
	}

	public void setPd1_list(ArrayList<Double> pd1_list) {
		this.pd1_list = pd1_list;
	}

	public ArrayList<Double> getPd2_list() {
		return pd2_list;
	}

	public void setPd2_list(ArrayList<Double> pd2_list) {
		this.pd2_list = pd2_list;
	}

	public ArrayList<Double> getPd3_list() {
		return pd3_list;
	}

	public void setPd3_list(ArrayList<Double> pd3_list) {
		this.pd3_list = pd3_list;
	}

	public ArrayList<Double> getPd4_list() {
		return pd4_list;
	}

	public void setPd4_list(ArrayList<Double> pd4_list) {
		this.pd4_list = pd4_list;
	}

	public boolean calcUsefulData(byte[] buffer) {
		data = new DataTranslation();
		if (dataExists(buffer)) {
			double pr = calcBufferVal(buffer[22], buffer[23], buffer[24],
					buffer[25]);
			Log.d(tag, "pr=" + pr);
			double res = calcBufferVal(buffer[18], buffer[19], buffer[20],
					buffer[21]);
			Log.d(tag, "res=" + res);
			Log.d(tag, "buffer[16]=" + String.valueOf(buffer[16]));
			Log.d(tag, "buffer[17]=" + String.valueOf(buffer[17]));

			if (dataNoError(pr, res, buffer[16], buffer[17])) {
				double T1 = 0, T2 = 0, T3 = 0, T4 = 0; // used to calculate pd1
														// to pd4
				double T5 = 0; // used to calculate thermistor
				status = 0;
				resolution = 0;
				pressure = 0;

				/*
				 * Bytes go from LSB to MSB Processed value = (Byte[1]*2^0) +
				 * (Byte[2]*2^8) + (Byte[3]*2^16) + (Byte[4]*2^24)
				 */
				for (int i = 0; i < 4; i++) {
					T1 += (buffer[i] & 0xff) << (8 * i);
				}

				for (int i = 4; i < 8; i++) {
					T2 += (buffer[i] & 0xff) << (8 * (i - 4));
				}

				for (int i = 8; i < 12; i++) {
					T3 += (buffer[i] & 0xff) << (8 * (i - 8));
				}

				for (int i = 12; i < 16; i++) {
					T4 += (buffer[i] & 0xff) << (8 * (i - 12));
				}

				for (int i = 16; i < 18; i++) {
					status += (buffer[i] & 0xff) << (8 * (i - 16)); // Status
				}

				for (int i = 18; i < 22; i++) {
					resolution += (buffer[i] & 0xff) << (8 * (i - 18)); // Resolution
				}

				for (int i = 22; i < 26; i++) {
					pressure += (buffer[i] & 0xff) << (8 * (i - 22));
				}

				volume = calcBufferVal(buffer[26], buffer[27], buffer[28],
						buffer[29]);

				for (int i = 30; i < 34; i++) {
					T5 += (buffer[i] & 0xff) << (8 * (i - 30));
				}

				pd1 = RoundData.roundFourDecimals((T1 * 3)
						/ (resolution * 4096));
				pd2 = RoundData.roundFourDecimals((T2 * 3)
						/ (resolution * 4096));
				pd3 = RoundData.roundFourDecimals((T3 * 3)
						/ (resolution * 4096));
				pd4 = RoundData.roundFourDecimals((T4 * 3)
						/ (resolution * 4096));
				thermistor = RoundData.roundFourDecimals((T5 * 3)
						/ (resolution * 4096));
				volume_list.add(volume);
				pd1_list.add(pd1);
				pd2_list.add(pd2);
				pd3_list.add(pd3);
				pd4_list.add(pd4);

				data.pd1 = pd1;
				data.pd2 = pd2;
				data.pd3 = pd3;
				data.pd4 = pd4;
				data.thermistor = thermistor;
				data.volume_list = volume_list;
				data.pd1_list = pd1_list;
				data.pd2_list = pd2_list;
				data.pd3_list = pd3_list;
				data.pd4_list = pd4_list;

				return true;
			}
			else {
				// Log.d("aaa-pr", String.valueOf(pr));
				// Log.d("aaa-buff", String.valueOf(buffer[17] & 0xff));
				// Log.d("aaa-res", String.valueOf(temp_res));
				// prevErrorIndex = currErrorIndex;
				// currErrorIndex = Metabolism.refCount;
				// if ((currErrorIndex - prevErrorIndex) == 1) {
				// errorCount++;
				// }
				// else {
				// if (errorCount > 0)
				// errorCount--;
				// }
				// Log.d("errorCount", String.valueOf(errorCount));
				// if (errorCount > 4) {
				// // quickDisconnect();
				// Log.d(tag,
				// "Connect function on after quick disconnect");
				// // Connect();
				// errorCount = 0;
				// }
				Log.d(tag, "some data get error.");
				return false;
			}

		}
		else {
			Log.d(tag, "All buffer data are zero.");
			return false;
		}
	}

	/**
	 * check whether there are some data in this buffer if one data is not 0,
	 * return true; otherwise, return false
	 */
	private boolean dataExists(byte[] buffer) {
		return !((buffer[0] & 0xff) == 0 && (buffer[1] & 0xff) == 0
				&& (buffer[2] & 0xff) == 0 && (buffer[3] & 0xff) == 0
				&& (buffer[4] & 0xff) == 0 && (buffer[5] & 0xff) == 0
				&& (buffer[6] & 0xff) == 0 && (buffer[7] & 0xff) == 0
				&& (buffer[8] & 0xff) == 0 && (buffer[9] & 0xff) == 0
				&& (buffer[10] & 0xff) == 0 && (buffer[11] & 0xff) == 0
				&& (buffer[12] & 0xff) == 0 && (buffer[13] & 0xff) == 0
				&& (buffer[14] & 0xff) == 0 && (buffer[15] & 0xff) == 0
				&& (buffer[16] & 0xff) == 0 && (buffer[17] & 0xff) == 0
				&& (buffer[18] & 0xff) == 0 && (buffer[19] & 0xff) == 0
				&& (buffer[20] & 0xff) == 0 && (buffer[21] & 0xff) == 0
				&& (buffer[22] & 0xff) == 0 && (buffer[23] & 0xff) == 0
				&& (buffer[24] & 0xff) == 0 && (buffer[25] & 0xff) == 0);
	}

	/**
	 * The processed data is correct only if: (1) Pressure! = 0 (2) Status == 0
	 * or 1 (3) Pressure in range of 600000 to 6000000 (4) Resolution != 0 (5)
	 * Resolution in range of 1000 to 10000. if all data is correct, return true
	 */
	private static boolean dataNoError(double pr, double res, byte buffer_16,
			byte buffer_17) {
		return (pr != 0) && (pr > 600000 && pr < 6000000)
				&& ((buffer_17 & 0xff) == 0)
				&& ((buffer_16 & 0xff) == 0 || (buffer_16 & 0xff) == 1)
				&& (res != 0) && (res > 1000 && res < 10000);
	}

	/** calculate data from bytes */
	private double calcBufferVal(byte a, byte b, byte c, byte d) {
		return RoundData.roundFourDecimals((a & 0xff)
				+ ((b & 0xff) * Math.pow(2, 8))
				+ ((c & 0xff) * Math.pow(2, 16))
				+ ((d & 0xff) * Math.pow(2, 24)));
	}

}

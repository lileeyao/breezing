package co.breezing.metabolism.parameter;

public class Parameter {

	// variable
	// device MAC address
	public static String MAC = "00:13:04:20:01:52";
	
	public static String gender = "male";
	public static int age = 25;
	public static int weight = 140;
	public static int height = 175;
	
	public static String qr_result = "0176511004881013700333400022100485000108192013201307220000000129012733";
	
}
